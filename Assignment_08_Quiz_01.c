#include <stdio.h>

struct student{
   char fn[30];
   char sub[75];
   int marks;
};

int main(){

  struct student students[5];

  printf("Enter Students Report Details\n ");
  for(int i=0;i<5;i++){
    printf("\nEnter the first name of the student: ");
    scanf("%s",students[i].fn);
    printf("\nEnter the subject: ");
    scanf("%s",students[i].sub);
    printf("\nEnter marks: ");
    scanf("%d",&students[i].marks);
  };

  printf("Display Student Report Details\n");
  for(int i=0;i<5;i++){
    printf("\nFirst name: %s",students[i].fn);
    printf("\nSubject: %s",students[i].sub);
    printf("\nMarks: %d",students[i].marks);
  };
  return 0;
};
